#include <stdio.h>
#include <time.h>
#include <windows.h>
#include <stdlib.h>
#include <conio.h>

#define M 80
#define N 26

char * unit(char (*arrFr)[3]);
void star(char(*arr)[M]);
void printS(char *arr, char *stop);

int main()
{
    srand(time(NULL) | clock());
    while (1)
    {
        char arr[N][M] = { 0 };
        star(arr);
        Sleep(1500);
        while (_kbhit())
            exit(0);
    }
    return 0;
}
void star(char(*arr)[M])
{
    char arrFr[3][3] = { 0 };
    char *pfr;
    int i, j, g, a;
    int count, stop;
    stop = rand() % 11 + 30;
    pfr = unit(arrFr);
    for (count = 0; count <= stop; count++)
    {
        i = rand() % (N/2 - 2);
        j = rand() % (M/2 - 2);
        g = 0; a = 0;
        for (g = 0; g < 9; g++)
        {
            if (a == 3)
            {
                a = 0; j++;
            }
            if (*(pfr + g))
            {
                arr[i + a][j] = *(pfr + g);
                arr[i + a][(M - 1) - j] = *(pfr + g);
                arr[(N - 1) - (i + a)][j] = *(pfr + g);
                arr[(N - 1) - (i + a)][(M - 1) - j] = *(pfr + g);
            }
            a++;
        }
    }
    printS(arr, arr + N);
}
char * unit(char (*arrFr)[3])
{
    int i, j;
    int count, stop;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
            arrFr[i][j] = 0;
    }
    stop = rand() % 2 + 3;
    for (count = 0; count <= stop; count++)
    {
        srand(time(NULL) | clock());
        i = rand() % 3;
        j = rand() % 3;
        if (arrFr[i][j])
            count--;
        else
            arrFr[i][j] = '*';
    }
    return arrFr;
}
void printS(char *arr, char *stop)
{
    system("cls");
    int i;
    for (i = (M - 1); arr<stop; i--, arr++)
    {
        if (*arr)
            putchar(*arr);
        else putchar(' ');
        if (!i)
        {
            putchar('\n');
            i = M;
        }
    }
}
