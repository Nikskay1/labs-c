// ��������� ���������� ��������� n_� ������� ���� ���������
// � ��������� ����������� ��� ����� �����
// ���������� ��������� �� ����� � ������������ � ����
#include <stdio.h>
#include <time.h>
#define N 40
int fib(int number);
void printFib(FILE * out);
int main()
{
	int n;
	FILE *out;
	
	if ((out = fopen("wF.txt", "w")) == NULL)  // ���� ��� ������
	{
		puts("error creating file\n");
		exit(0);
	}

	printFib(out);

	if (fclose(out) != 0)
		puts("error closing files.\n");

	return 0;
}
int fib(int number)                 // ����������� �������
{
	if (number == 1 || number == 2)
		return 1;
	else
		return fib(number - 1) + fib(number - 2);
}
void printFib(FILE * out)         // ����� � ������
{
	int number;
	int n;
	double timFib;
	clock_t time_start, time_end;
	for (number = 1; number <= N; number++)
	{
		time_start = clock();
		n = fib(number);
		time_end = clock();             // ����������� �������
		
		printf("%d - %d", number, n);
		printf(" t _ %.3lfc\n", timFib = (double)(time_end - time_start) / CLOCKS_PER_SEC);
		fprintf(out, "%d - %d t _ %.3lfc\n", number, n, timFib);
	}

}