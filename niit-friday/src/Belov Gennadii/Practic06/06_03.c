// ��������� ��������� ����� ����� � ������
#include <stdio.h>
typedef long long LL;
void funConverting(char * pArr, LL number, int *ind);
void printS(char *pArr, int *ind);
int main()
{
	char arr[80];
	int ind = 0;
	LL number;
	printf("number: ");
	while (scanf("%lld", &number) != 1)
	{
		printf("error\nnumber: ");
		while (getchar() != '\n')
			continue;
	}
	if (number < 0)              // ��� �������������� �����
	{
		arr[0] = '-';
		ind = 1;
		number = 0 - number;
	}
	funConverting(arr, number, &ind);
	arr[ind] = '\0';                  // ����� ������
	printS(arr, &ind);
	return 0;
}
void funConverting(char *pArr, LL number, int *ind)   // �������������� � �������� �������
{
	if (number<10)
		*(pArr + *ind) = number + '0';
	else
	{
		funConverting(pArr, number / 10, ind);
		*(pArr + *ind) = number % 10 + '0';
	}
	(*ind)++;
}
void printS(char *pArr, int*ind)          // ������
{
	int i;
	for (i = 0; i<=*ind; i++)
		printf("arr[%d] = %c _ %d\n", i, *(pArr + i), *(pArr + i));
	printf("str: %s\n", pArr);
}