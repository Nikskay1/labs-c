// ��������� ������� ����� �� ���������
// �� ������� ������ ����
#include <stdio.h>
#include <time.h>
#include <windows.h>
#define M 9
#define N 29
void createLr(char *pStart, FILE *lrint);
void printL(char *arr, char *stop);
int road(int x, int y, char(*el)[N], int *step, char *route);

int main()
{
	srand(time(NULL) | clock());
	char arr[M][N];
	FILE *lrint;
	int x, y;   
	int step = 0;
	char route = 'N';
	if ((lrint = fopen("l.txt", "r")) == NULL)  // ���� ���������
	{
		puts("error creating file\n");
		exit(0);
	}
	createLr(arr, lrint);
	printL(arr, arr + M);
	do
	{
		x = rand() % 4 + M / 2;
		y = rand() % 4 + N / 2;
	} while (arr[x][y] != ' ');    // ���������� ���������� ���������
	printf("\n%d step\n", road(x, y, arr, &step, &route));  // ���-�� ���������� ����� �� ������

	if (fclose(lrint) != 0)
		puts("error closing files.\n");
	return 0;
}
void createLr(char *pStart, FILE *lrint)     // �������� (������) ���������
{
	
	while ((*pStart++ = getc(lrint)) != EOF);
	
}
void printL(char *arr, char *stop)       // ����������� ��������� � ��������
{
	system("cls");
	int i;
	for (i = (N - 1); arr<stop; i--, arr++)
	{
		if (*arr)
			putchar(*arr);
		else putchar(' ');

		if (!i)
		{
			putchar('\n');
			i = N;
		}
	}
}
int road(int x, int y, char(*el)[N], int *step, char *route)   // ���� �� ���������
{
	int fl=0;
	
	el[x][y] = 'x';
	(*step)++;
	printL(el, el + M);
	printf("%d", *step-1);     // ���-�� ���������� �����
		
	Sleep(250);
	if (x == 0 || y == 0 || x == N - 1 || y == N - 1)   // ����������� ������� ���������
		return 0;
	switch (*route)       // ��������� ����������� �������� � ����������� �� ���������� ����������
	{
	case 'N':

		while (fl < 2)     // ������ ������ ���� ��� "������" �����
		{

			if (el[x - 1][y] != '#' && (el[x - 1][y] != '.' || fl))   // �������� �� ����������� �������
			{
				el[x][y] = '.';
				*route = 'N';
				return road(x - 1, y, el, step, route) + 1;
			}
			if (el[x][y + 1] != '#' && (el[x][y + 1] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'E';
				return road(x, y + 1, el, step, route) + 1;
			}
			if (el[x + 1][y] != '#' && (el[x + 1][y] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'S';
				return road(x + 1, y, el, step, route) + 1;
			}
			if (el[x][y - 1] != '#' && (el[x][y - 1] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'W';
				return road(x, y - 1, el, step, route) + 1;

			}
			fl++;
		}
	case 'E':
		while (fl < 2)
		{
			if (el[x][y + 1] != '#' && (el[x][y + 1] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'E';
				return road(x, y + 1, el, step, route) + 1;
			}
			if (el[x + 1][y] != '#' && (el[x + 1][y] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'S';
				return road(x + 1, y, el, step, route) + 1;
			}
			if (el[x][y - 1] != '#' && (el[x][y - 1] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'W';
				return road(x, y - 1, el, step, route) + 1;

			}
			if (el[x - 1][y] != '#' && (el[x - 1][y] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'N';
				return road(x - 1, y, el, step, route) + 1;
			}
			fl++;
		}
	case 'S':
		while (fl < 2)
		{
			if (el[x + 1][y] != '#' && (el[x + 1][y] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'S';
				return road(x + 1, y, el, step, route) + 1;
			}
			if (el[x][y - 1] != '#' && (el[x][y - 1] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'W';
				return road(x, y - 1, el, step, route) + 1;

			}
			if (el[x - 1][y] != '#' && (el[x - 1][y] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'N';
				return road(x - 1, y, el, step, route) + 1;
			}
			if (el[x][y + 1] != '#' && (el[x][y + 1] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'E';
				return road(x, y + 1, el, step, route) + 1;
			}
			fl++;
		}
	case 'W':
		while (fl < 2)
		{
			if (el[x][y - 1] != '#' && (el[x][y - 1] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'W';
				return road(x, y - 1, el, step, route) + 1;

			}
			if (el[x - 1][y] != '#' && (el[x - 1][y] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'N';
				return road(x - 1, y, el, step, route) + 1;
			}
			if (el[x][y + 1] != '#' && (el[x][y + 1] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'E';
				return road(x, y + 1, el, step, route) + 1;
			}
			if (el[x + 1][y] != '#' && (el[x + 1][y] != '.' || fl))
			{
				el[x][y] = '.';
				*route = 'S';
				return road(x + 1, y, el, step, route) + 1;
			}
			fl++;
		}
	}
	
}