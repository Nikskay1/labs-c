#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define N 80
void printWord(char *p);
int getWords(char *str1, char **p);

void Print_words(char str1[], FILE *fp)
{
	int i = 0, numb_words, k;
	char *p[N], *temp;
	
	i = strlen(str1) - 1;
	str1[i] = ' ';
	srand(time(0));
	numb_words = getWords(str1, p);

	for (i = numb_words; i > 0; --i)
	{
		k = rand() % i;
		printWord(p[k],fp);
		temp = p[k];
		p[k] = p[i - 1];
		p[i - 1] = temp;
	}
}
int getWords(char *str, char **p)
{
	int i = 0, j = 0, inWord = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			p[j] = str + i;
			j++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	return j;
}
void printWord(char *p, FILE *fp)
{
	while (*p != ' ')
	{
		putc(*p++,fp);
	}
	putc(' ',fp);
}