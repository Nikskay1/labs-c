//This program removes excess spaces from a string
#include <stdio.h>
#include <stdlib.h>
#define N 80


int main()
{
	int i = 0, flag = 0, j=0,length=0;
	char str[N];
	printf("Please, print something \n");
	fgets(str, 80, stdin);

	length = strlen(str) - 1;
	str[length] = '\0';
//deleting spaces in the beginning
		while (str[0] == ' ')
	{
		for (j = 0; j<length; ++j)
			str[j] = str[j + 1];
	}
//deleting spaces in the end
	i = length-1;
	while (str[i] == ' ')
	{
		str[i] = str[i + 1];
		--i;
	}

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] != ' ')
		{
			i++; flag = 0;
		}
		else if (flag == 0)
		{
			i++; flag = 1;
		}
		else 
		{
			for (j = i; str[j] != '\0'; ++j)
				str[j] = str[j + 1];
		}
		
	}
	printf("New string \n");
	for (i = 0; str[i] != '\0';++i)
		printf("%c", str[i]);
	
	printf("\n ");

	return 0;
}
