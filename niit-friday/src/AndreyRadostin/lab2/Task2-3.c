//This program prints triangles of stars
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int get_int();// function controling the integer input
int main()
{
	int strn = 0, i = 0, j = 0;
	printf("Please, enter a number of strings\n");
	strn = get_int();
	for (i = 1; i < strn + 1; ++i)
	{
		for (j = 0; j < strn - i; ++j)
			printf(" ");
		for (j = 0; j < 2 * i - 1; ++j)
			printf("*");
		printf("\n");
	}
	return 0;
}

int get_int()
{

	int input;
	char ch;
	while (scanf("%d", &input) != 1)
	{
		while ((ch = getchar()) != '\n')
			putchar(ch); // exemption from incorrect character
		printf(" is not integer. \n Please, input ");
		printf(" integer number: ");
	}
	return input;

}