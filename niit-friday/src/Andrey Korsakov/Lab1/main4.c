/*
�������� ���������, ������� ��������� ���� �� ������������ ��-
����� (����, �����) � ����������� (����������). ������ ���-
����� � ���� ���� ����� �����, ��������� � ���� �������������
����� � ��������� �� 1 �����. 1 ��� = 12 ������. 1 ���� = 2.54 ��.
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	float height = 0;
	float result = 0;

	printf("Hello, enter your height (ft.in)\n");
	scanf("%f", &height);
	if (height > 0)
	{
		result = height * 12 * 2.54;
		printf("Your height %.1lf cm\n", result);
	}
	else
		printf("Sorry, repeat again\n");				//��������� �����
	
	return(0);
}