/*
�������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ���������� -
��� � ��������� ������������� ����������.
���������:
������ ����������� ���������� ������� : �������������� � ������������ -
�� �������(��� ����� �������...)
*/

#include <stdio.h>
#include <time.h>

char str[80] = { 0 };
int i = 0;
int N = 0;
int sum = 0;
int ran = 0;

int main()
{
	printf("Enter a number between 5 and 80:\n");
	scanf("%d", &N);

	while (N < 5 || N > 80)
	{
		printf("Sorry, repeat again:\n");		//��������� �����
		fflush(stdin);
		scanf("%d", &N);
	}

	srand(time(0));

	while (i < N)
	{
		ran = 1 + rand() % 9;
		if (i % 2 == 0)
			str[i] = ran;
		else
			str[i] = (-ran);
		printf("%d, ", str[i]);
		i++;
	}

	for (i = 0; str[i] > 0; i++)
	{
	}

	while (str[N - 1] < 0)
	{
		N--;
	}

	while ((i) < (N - 2))
	{
		i++;
		sum = str[i] + sum;
	}

	printf("\nSum:%d\n", sum);

	return 0;
}