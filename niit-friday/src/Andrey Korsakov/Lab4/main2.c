/*
�������� ���������, ������� � ������� ������� ���������� ������� �����
������ � �������� �������
���������:
������ �� ������� ��������� ������ ���������� �� char, � ������� ���������
������ ������ �������� ������� ����� (������������ - ������ ������� � ��-
�������� ��������). ����� �� ���������� ����� ����� ������, ��������� ����
������ �� ����������.
*/
#include <stdio.h>

#define N 80
#define M 80

char str[M];
char *p[N];
int i = 0, j = 0, count = 0, inWord = 0;

int main()
{
	printf("Enter an arbitrary string: \n");	//������� ������������ ������
	fgets(str, 80, stdin);

	while (str[i] != '\n' && j < N && i < (M - 1))
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			p[j] = &str[i];
			j++;
		}
		else if ((str[i] == ' ') && inWord == 1)
		{
			inWord = 0;
			p[j] = &str[i - 1];
			j++;
		}

		i++;
	}
	if (inWord == 1)
		p[j] = &str[i - 1];
	else
		j--;

	for (; j >= 0; j -= 2)
	{
		while (p[j] >= p[j - 1])
		{
			putchar(*p[j-1]);
			p[j - 1]++;
		}
		putchar(' ');
	}
	putchar('\n');
	
	return 0;
}